@extends('layouts.app')

@section('content')


<div class="panel panel-default m-3">
    <form action="{{ route('update') }}" method="POST">
        @csrf
        <div class="row">
            <div class="col-md-6">
                <label for=""><strong>Datos de Instagram:</strong></label>
                <p>Tiene que ser el nombre de usuario (@nombredeusuario) de la cuenta que quieres aumentar el crecimiento.</p>
                <label for="cuenta">Nombre de Usuario:</label>
                <input required class="form-control col-lg-6 mb-3" type="text" name="cuenta" id="cuenta" placeholder="nombredeusuario" value="{{ Auth::user()->cuenta }}">
                <label for="igpassword">Contraseña:</label>
                <input required class="form-control col-lg-6 mb-3" type="password" name="igpassword" id="igpassword" placeholder="Contraseña de Instagram">
                <p>La misma contraseña que usas para iniciar sesión en el app.</p>
                <p><strong>La contraseña es encriptada</strong>, ninguna persona va a poder ver la misma. Pero es necesaria para que el sistema pueda conectarse a tu cuenta y funcionar.</p>
            </div>
            <div class="col-md-6">
                <label for=""><strong>Hastags# de Referencia:</strong></label>
                <p>Sin usar el #</p>
                <br>
                <div class="row">
                    <div class="col-lg-6">
                        <label for="hashtag1">Hashtag 1:</label>
                        <input required name="hashtag1" id="hashtag1" class="form-control   mb-3" type="text" placeholder="homedeco" value="{{ Auth::user()->hashtag1 }}">
                        <label for="hashtag2">Hashtag 3:</label>
                        <input name="hashtag3" id="hashtag3" class="form-control   mb-3" type="text" placeholder="diseño" value="{{ Auth::user()->hashtag3 }}">
                    </div>
                    <div class="col-lg-6">
                        <label for="hashtag3">Hashtag 2:</label>
                        <input required name="hashtag2" id="hashtag2" class="form-control   mb-3" type="text" placeholder="diseñodeinteriores" value="{{ Auth::user()->hashtag2 }}">
                        <label for="hashtag4">Hashtag 4:</label>
                        <input name="hashtag4" id="hashtag4" class="form-control   mb-3" type="text" placeholder="argentina" value="{{ Auth::user()->hashtag4 }}">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <label for="referencias">Cuentas de Referencia:</label>
                <p>Los usuarios de tus competidores o cuentas de referencias sin usar @ y también tienen que tener +5000 followers para que el sistema funcione bien.</p>
                <label for="referencia1">Referencia 1:</label>
                <input required name="referencia1" id="referencia1" class="form-control mb-3" type="text" placeholder="clarincom" value="{{ Auth::user()->referencia1 }}">
                <label for="referencia2">Referencia 2:</label>
                <input required name="referencia2" id="referencia2" class="form-control mb-3" type="text" placeholder="lanacioncom" value="{{ Auth::user()->referencia2 }}">
                <label for="referencia3">Referencia 3:</label>
                <input name="referencia3" id="referencia3" class="form-control mb-3" type="text" placeholder="otracuenta" value="{{ Auth::user()->referencia3 }}">
                <label for="referencia4">Referencia 4:</label>
                <input name="referencia4" id="referencia4" class="form-control mb-3" type="text" placeholder="otracuenta" value="{{ Auth::user()->referencia4 }}">
            </div>
            <div class="col-lg-4 col-md-6">
                <p>Comentarios:</p>
                <a class="btn-sm btn-primary btn-icon-split mb-3" data-toggle="modal" data-target="#exampleModal">
                    <span class="icon text-gray-600">
                      <i class="fas fa-arrow-right text-white"></i>
                    </span>
                    <span class="text text-white">Cargar Comentario</span>
                  </a>
                <ol>
                    <li>{{Auth::user()->customizations()[0]->comment1}}</li>
                    <li>{{Auth::user()->customizations()[0]->comment2}}</li>
                    <li>{{Auth::user()->customizations()[0]->comment3}}</li>
                    <li>{{Auth::user()->customizations()[0]->comment4}}</li>
                    <li>{{Auth::user()->customizations()[0]->comment5}}</li>
                    <li>{{Auth::user()->customizations()[0]->comment6}}</li>
                    <li>{{Auth::user()->customizations()[0]->comment7}}</li>
                    <li>{{Auth::user()->customizations()[0]->comment8}}</li>
                    <li>{{Auth::user()->customizations()[0]->comment9}}</li>
                    <li>{{Auth::user()->customizations()[0]->comment10}}</li>
                </ol>
                <p>Customiza el sistema a tus necesidades:</p>
                <ul>
                  @if(!Auth::user()->customizations()[0]->follow)
                    <li><label for=""><input name="follow" id="follow" type="checkbox"> Seguir y Dejar de Seguir</label></li>
                  @else
                    <li><label for=""><input name="follow" id="follow" type="checkbox" checked> Seguir y Dejar de Seguir</label></li>
                  @endif
                  @if(!Auth::user()->customizations()[0]->likes)
                    <li><label for=""><input name="likes" id="likes" type="checkbox"> Likes</label></li>
                  @else
                    <li><label for=""><input name="likes" id="likes" type="checkbox" checked> Likes</label></li>
                  @endif
                  @if(!Auth::user()->customizations()[0]->comment)
                    <li><label for=""><input name="comment" id="comment" type="checkbox"> Comentarios</label></li>
                  @else
                    <li><label for=""><input name="comment" id="comment" type="checkbox" checked> Comentarios</label></li>
                  @endif
                  @if(!Auth::user()->customizations()[0]->private)
                    <li><label for=""><input name="private" id="private" type="checkbox"> Ignorar Usuarios Privados</label></li>
                  @else
                    <li><label for=""><input name="private" id="private" type="checkbox" checked> Ignorar Usuarios Privados</label></li>
                  @endif
                </ul>
                
            </div>
            <div class="col-md-4">
                <p>El usuario de Instagram es el que resaltamos en la imagen de ejemplo acá abajo:</p>
                <img class="m-auto" style="width: 250px; display:block" src="/img/userExample.png" alt="">
                <div class="d-flex justify-content-center flex-wrap">
                </div>
            </div>
        </div>
        <div class="d-flex justify-content-center">
          <button class="btn-lg btn-primary mt-5" type="submit">Confirmar Cambios</button>
        </div>
      </div>
      
      
      <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Cargar Comentario</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              
              <div class="form-group">
                <label for="comment1" class="col-form-label">Comentario 1:</label>
                <textarea class="form-control" name="comment1" id="comment1">{{Auth::user()->customizations()[0]->comment1}}</textarea>
              </div>
              <div class="form-group">
                <label for="comment2" class="col-form-label">Comentario 2:</label>
                <textarea class="form-control" name="comment2" id="comment2">{{Auth::user()->customizations()[0]->comment2}}</textarea>
              </div>
              <div class="form-group">
                <label for="comment3" class="col-form-label">Comentario 3:</label>
                <textarea class="form-control" name="comment3" id="comment3">{{Auth::user()->customizations()[0]->comment3}}</textarea>
              </div>
              <div class="form-group">
                <label for="comment4" class="col-form-label">Comentario 4:</label>
                <textarea class="form-control" name="comment4" id="comment4">{{Auth::user()->customizations()[0]->comment4}}</textarea>
              </div>
              <div class="form-group">
                <label for="comment5" class="col-form-label">Comentario 5:</label>
                <textarea class="form-control" name="comment5" id="comment3">{{Auth::user()->customizations()[0]->comment5}}</textarea>
              </div>
              <div class="form-group">
                <label for="comment6" class="col-form-label">Comentario 6:</label>
                <textarea class="form-control" name="comment6" id="comment3">{{Auth::user()->customizations()[0]->comment6}}</textarea>
              </div>
              <div class="form-group">
                <label for="comment7" class="col-form-label">Comentario 7:</label>
                <textarea class="form-control" name="comment7" id="comment3">{{Auth::user()->customizations()[0]->comment7}}</textarea>
              </div>
              <div class="form-group">
                <label for="comment8" class="col-form-label">Comentario 8:</label>
                <textarea class="form-control" name="comment8" id="comment3">{{Auth::user()->customizations()[0]->comment8}}</textarea>
              </div>
              <div class="form-group">
                <label for="comment9" class="col-form-label">Comentario 9:</label>
                <textarea class="form-control" name="comment9" id="comment3">{{Auth::user()->customizations()[0]->comment9}}</textarea>
              </div>
              <div class="form-group">
                <label for="comment10" class="col-form-label">Comentario 10:</label>
                <textarea class="form-control" name="comment10" id="comment3">{{Auth::user()->customizations()[0]->comment10}}</textarea>
              </div>
              
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary" data-dismiss="modal">Continuar</button>
            </form>
      </div>
    </div>
  </div>
</div>

@endsection